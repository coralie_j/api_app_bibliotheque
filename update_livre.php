<?php

    if(isset($_GET['titre'],$_GET['auteur'],$_GET['editeur'],$_GET['nb'])) {

      $servername = '127.0.0.1';
      $username = 'coralie';
      $password = 'root';

      try{

        // Connexion à la base de données
        $bdd = new PDO("mysql:host=$servername;dbname=bibliotheque", $username, $password);
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Si le livre est enregistré dans la base de données, je mets à jour son nombre d'exemplaires
        $requete_update = "UPDATE exemplaire SET nb_exemplaires = nb_exemplaires + :nb_exemplaires_recu
                           WHERE id_livre=ANY(
                           SELECT id FROM livre
                           WHERE titre=:titre AND auteur=:auteur AND editeur=:editeur)";

        $livre_existant_requete = $bdd->prepare($requete_update);

        $livre_existant_requete->bindValue(":titre",$_GET['titre']);
        $livre_existant_requete->bindValue(":auteur",$_GET['auteur']);
        $livre_existant_requete->bindValue(":editeur",$_GET['editeur']);
        $livre_existant_requete->bindValue(":nb_exemplaires_recu",$_GET['nb']);
        $livre_existant_requete->execute();

        $ligne_modifie = $livre_existant_requete->rowCount();

        // Renvoi d'un tableau JSON en fonction de la modification de la table exemplaire via la mise à jour
        if($ligne_modifie == 1){
          echo json_encode(array('update' => true));
        } else {
          echo (json_encode(array("update"=>false)));
        }
      }catch(PDOException $e){
              echo "Erreur : " . $e->getMessage();
      }
      $bdd = null; // Déconnexion de la base de données
    }


 ?>
