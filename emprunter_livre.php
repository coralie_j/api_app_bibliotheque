<?php

    $servername = '127.0.0.1';
    $username = 'coralie';
    $password = 'root';
    if (isset($_GET['nom'],$_GET['prenom'],$_GET["email"],$_GET["tel"],$_GET['date_retour'],$_GET['id_livre'])){

      try{
          // Connexion à la base de données
          $conn = new PDO("mysql:host=$servername;dbname=bibliotheque", $username, $password);
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

          // Vérification de l'existance de l'utilisateur dans la base de données
          $util_existe = $conn->prepare("SELECT id FROM utilisateur WHERE nom=? AND prenom=? AND email=? AND telephone=?");
          $util_existe->bindValue(1, $_GET['nom']);
          $util_existe->bindValue(2, $_GET['prenom']);
          $util_existe->bindValue(3, $_GET["email"]);
          $util_existe->bindValue(4, $_GET['tel']);
          $util_existe->setFetchMode(PDO::FETCH_ASSOC);
          $util_existe->execute();
          $id_util = $util_existe->fetch(); // Récupération de l'identifiant de l'utilisateur

          // Si l'utilisateur n'existe pas, je l'insère dans la table utilisateur
          if(empty($id_util)){
            $insert_util = $conn->prepare('INSERT INTO utilisateur(nom,prenom,email,telephone) VALUES (?,?,?,?)');
            $insert_util->bindValue(1, $_GET['nom']);
            $insert_util->bindValue(2, $_GET['prenom']);
            $insert_util->bindValue(3, $_GET["email"]);
            $insert_util->bindValue(4, $_GET['tel']);
            $insert_util->execute();
            $id_util = $conn->lastInsertId(); // Récupération de l'id de l'utilisateur inséré
          }

          // Enregistrement de l'emprunt
          $insert_emprunt = $conn->prepare('INSERT INTO emprunt VALUES (:id_util,:id_livre,CURRENT_DATE(),:date_retour)');
          $insert_emprunt->bindValue(':id_util',$id_util['id']);
          $insert_emprunt->bindValue(':date_retour', $_GET['date_retour']);
          $insert_emprunt->bindValue(':id_livre', $_GET['id_livre']);
          $insert_emprunt->setFetchMode(PDO::FETCH_ASSOC);
          $insert_emprunt->execute();

          // Mise à jour du nombre d'exemplaires
          $update = $conn->prepare("UPDATE exemplaire SET nb_exemplaires = nb_exemplaires-1 WHERE id_livre=:id");
          $update->bindValue(":id", $_GET['id_livre']);
          $update->execute();
          $conn = null;

        }catch(PDOException $e){

          echo "Erreur : " . $e->getMessage();
        }

  }




 ?>
