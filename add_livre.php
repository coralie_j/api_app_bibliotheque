<?php

  if(isset($_GET['titre'],$_GET['auteur'],$_GET['editeur'],$_GET['genre'],$_GET['annee'],$_GET['nb'])) {

      $servername = '127.0.0.1';
      $username = 'coralie';
      $password = 'root';

      try{

        // Connexion à la base de données
        $bdd2 = new PDO("mysql:host=$servername;dbname=bibliotheque", $username, $password);
        $bdd2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Insertion d'un nouveau livre dans la base de données ainsi que le nombre d'exemplaire disponible
        $ajout_livre = $bdd2->prepare("INSERT INTO livre(titre,auteur,editeur,genre,annee_parution) VALUES(?,?,?,?,?)");
        $ajout_livre->bindValue(1,$_GET['titre']);
        $ajout_livre->bindValue(2,$_GET['auteur']);
        $ajout_livre->bindValue(3,$_GET['editeur']);
        $ajout_livre->bindValue(4,$_GET['genre']);
        $ajout_livre->bindValue(5,$_GET['annee']);
        $ajout_livre->setFetchMode(PDO::FETCH_ASSOC);
        $ajout_livre->execute();
        $id_livre = $bdd2->lastInsertId(); // Récupération de l'id du nouveau livre ajouté

        $ajout_exemplaire = $bdd2->prepare("INSERT INTO exemplaire VALUES(?,?)");
        $ajout_exemplaire->bindValue(1,$id_livre);
        $ajout_exemplaire->bindValue(2,$_GET['nb']);
        $ajout_exemplaire->execute();

      }catch(PDOException $e){
              echo "Erreur : " . $e->getMessage();
      }
      $bdd2 = null; // Déconnexion base de données
    }
 ?>
