<?php

		if (isset($_GET['titre']) && isset($_GET['auteur'])) {

	    		$servername = '127.0.0.1';
      		$username = 'coralie';
      		$password = 'root';

      try{
							// Connexion à la base de données
              $conn = new PDO("mysql:host=$servername;dbname=bibliotheque", $username, $password);
              $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

							if($_GET['editeur']==""){
								unset($_GET['editeur']);
							}

							// Rechercher les livres qui correspondent aux critères (auteur,titre et editeur (optionnel))
						  $recup_id = $conn->prepare('SELECT id,editeur FROM livre WHERE titre = :titre AND auteur = :auteur AND (:editeur IS NULL OR editeur = :editeur)');
						  $recup_id->bindValue(":titre", $_GET['titre']);
						  $recup_id->bindValue(":auteur", $_GET['auteur']);
						  $recup_id->bindValue(":editeur", $_GET['editeur']);
              $recup_id->setFetchMode(PDO::FETCH_ASSOC);
              $recup_id->execute();
						 	$id = $recup_id->fetchAll(); // Récupération des infos sur les livres

							/*
							Si le livre n'est pas dans la table livre, on renvoie un tableau JSON avec une clé "nb_exemplaires" et une valeur -1.
							Sinon, on renvoie un tableau JSON contenant les clés editeur,id,nb_exemplaires avec les informations des livres.
							 */

						 if (empty($id)){
							 $nb_exemplaires = [array("nb_exemplaires"=>"-1")];
							 echo (json_encode($nb_exemplaires));

						 } else {

							 $taille = sizeof($id);

							 $nb_exemplaires = array();
							 for($i=0;$i<$taille;$i++){
								 $requete_exemplaire = $conn->prepare("SELECT nb_exemplaires FROM exemplaire WHERE id_livre=".$id[$i]['id']);
								 $requete_exemplaire->setFetchMode(PDO::FETCH_ASSOC);
	               $requete_exemplaire->execute();
								 array_push($nb_exemplaires,array_merge($requete_exemplaire->fetch(),$id[$i]));
							 }

							 echo json_encode($nb_exemplaires);
						 }

						 $conn = null; // Déconnexion à la base de données



            }
            catch(PDOException $e){
              echo "Erreur : " . $e->getMessage();
            }
					}


 ?>
