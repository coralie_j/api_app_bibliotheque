<?php
    header('Content-Type: application/json; charset=utf-8'); // Initialisation de l'en-tête de l'API

    $servername = '127.0.0.1';
    $username = 'coralie';
    $password = 'root';

    // Récupération des paramètres
    $data = json_decode(file_get_contents('php://input'), true);

    try {
          // Connexion à la base de données
          $bdd = new PDO("mysql:host=$servername;dbname=bibliotheque", $username, $password);
          $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

          // Vérification des login et mot de passe
          $recup_id = $bdd->prepare('SELECT id FROM acces WHERE login=:login AND mdp=MD5(:mdp)');
          $recup_id->bindValue(":login",$data["login"]);
          $recup_id->bindValue(":mdp",$data['password']);
          $recup_id->setFetchMode(PDO::FETCH_ASSOC);
          $recup_id->execute();
          $id_admin = $recup_id->fetch();

          // Renvoi de la validité des login et mdp
          if (! $id_admin){
            echo(json_encode(['accès'=>false]));
          } else {
            echo(json_encode(['accès'=>true]));
          }


    } catch(PDOException $e){
              echo "Erreur : " . $e->getMessage();
    }
    $bdd = null;


 ?>
